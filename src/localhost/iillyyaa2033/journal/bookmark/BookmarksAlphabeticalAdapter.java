package localhost.iillyyaa2033.journal.bookmark;

import android.content.Context;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import java.util.ArrayList;
import java.util.Arrays;
import localhost.iillyyaa2033.journal.R;
import localhost.iillyyaa2033.journal.model.JournalEntry;

public class BookmarksAlphabeticalAdapter extends BaseAdapter {

	public static final String SECTION_PREFIX = "¡";

	private Handler h = new Handler();

	private String[] items = {};
	private LayoutInflater inflater;
	
	public BookmarksAlphabeticalAdapter(Context c) {
		inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public void set(final JournalEntry[] items) {
		new Thread(){

			@Override
			public void run() {
				Arrays.sort(items);

				ArrayList<String> list = new ArrayList<String>();
				char sectionLetter = ' ';
				String currentTheme = null;
				for (int i = 0; i < items.length; i ++) {
					JournalEntry entry = items[i];
					String header = entry.header;
					
					if (!entry.isDiaryEntry() && header != null) {
						header = header.toUpperCase();
						
						char currentChar = header.charAt(0);
						if (currentChar != sectionLetter) {
							list.add(SECTION_PREFIX + currentChar);
							sectionLetter = currentChar;
						}

						if (!header.equals(currentTheme)) {
							list.add(entry.header);
							currentTheme = header;
						}
					}
				}

				doSet(list.toArray(new String[list.size()]));
			}
		}.start();
	}

	private void doSet(final String[] items) {
		h.post(new Runnable(){

				@Override
				public void run() {
					BookmarksAlphabeticalAdapter.this.items = items;
					notifyDataSetChanged();
				}
			});
	}

	@Override
	public int getCount() {
		return items.length;
	}

	@Override
	public String getItem(int p1) {
		return items[p1];
	}

	public String getItemClear(int p1) {
		String item = getItem(p1);
		if (item != null) {
			if (item.startsWith(SECTION_PREFIX)) {
				item = item.substring(SECTION_PREFIX.length());
			}
		}

		return item;
	}

	@Override
	public long getItemId(int p1) {
		return p1;
	}

	@Override
	public View getView(int p1, View p2, ViewGroup p3) {
		String item = getItem(p1);

		boolean isSection = false;
		if (item.startsWith(SECTION_PREFIX)) {
			isSection = true;
			item = item.substring(SECTION_PREFIX.length());
		}

		View v = inflater.inflate(isSection ? R.layout.list_divider : R.layout.list_item, null);

		TextView t = (TextView) v.findViewById(R.id.text1);
		t.setText(item);

		return v;
	}

	public boolean isSection(int p1){
		return getItem(p1).startsWith(SECTION_PREFIX);
	}
}
