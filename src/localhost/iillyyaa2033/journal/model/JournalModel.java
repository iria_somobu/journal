package localhost.iillyyaa2033.journal.model;

import android.graphics.Color;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.text.style.ForegroundColorSpan;
import android.text.style.URLSpan;
import android.view.View;
import android.widget.TextView;
import java.util.List;
import ru.appheads.pagesplitter.PageSplitter;

public class JournalModel {

	public static boolean useTESDate = false;

	private TextView[] views;

	private JournalEntry[] journal = {};
	private CharSequence[] pages = {};
	private int currentPageIndex = 1;
	private boolean showAllTitles = true;

	private UpdateListener listener = null;

	public JournalModel(TextView... views) {
		this.views = views;
		breakFor(views[0]);
	}

	public void prev() {
		currentPageIndex -= views.length;
		if (currentPageIndex <= 1) {
			currentPageIndex = 1;
		}
		show();
	}

	public void next() {
		currentPageIndex += views.length;
		show();
	}

	public int getPagesCount(){
		return journal.length;
	}
	
	public int getCurrentPageNum() {
		return currentPageIndex;
	}

	public void setCurrentPageNum(int num) {
		currentPageIndex = num;
		show();
	}

	public boolean showPrev() {
		return currentPageIndex > 1;
	}

	public boolean showNext() {
		return currentPageIndex <= pages.length - views.length;
	}

	public void set(JournalEntry[] entries, boolean allTitles) {
		journal = entries;
		this.showAllTitles = allTitles;
		breakFor(views[0]);
		show();
	}

	public void setUpdList(UpdateListener l) {
		this.listener = l;
	}

	private void breakFor(TextView view) {
		int width = view.getWidth();
		int height = view.getHeight();

		boolean isFirst = true;
		
		PageSplitter splitter = new PageSplitter(view.getWidth(), view.getHeight(), view.getLineSpacingMultiplier(), (int)view.getLineSpacingExtra());
		for (JournalEntry entry : journal) {
			splitter.append(entry.toString(isFirst || showAllTitles)+"\n\n", view.getPaint());
			isFirst = false;
		}
		List<CharSequence> pages = splitter.getPages();

		this.pages = pages.toArray(new CharSequence[pages.size()]);

		if (showAllTitles) {
			currentPageIndex = this.pages.length - (width > height ? 1 : 0);
			if (currentPageIndex < 1) currentPageIndex = 1;
		}
	}

	private void show() {
		int pg = currentPageIndex - views.length;
		if (pg < 0) {
			pg = 0;
		}

		for (TextView view : views) {
			show(view, pg++);
		}
	}

	private void show(TextView view, int page) {
		if (page < pages.length) {
			view.setText(getSpanned(pages[page].toString()));
		} else {
			view.setText("");
		}
	}

	private Spanned getSpanned(String source) {
		String result = source.replace("[[", "<a href=\"nothing\">");
		result = result.replace("]]", "</a>");
		result = result.replace("\n", "<br/>");
		result = result.replace("‹‹", "<font color=\"#A61011\">");
		result = result.replace("››", "</font>");

		CharSequence spannedText = Html.fromHtml(result);

		SpannableStringBuilder strBuilder = new SpannableStringBuilder(spannedText);
		URLSpan[] urls = strBuilder.getSpans(0, spannedText.length(), URLSpan.class);   
		for (URLSpan span : urls) {
			makeLinkClickable(strBuilder, span);
		}

		return strBuilder;
	}

	private void makeLinkClickable(SpannableStringBuilder strBuilder, final URLSpan span) {
		int start = strBuilder.getSpanStart(span);
		int end = strBuilder.getSpanEnd(span);
		int flags = strBuilder.getSpanFlags(span);

		char[] chars = new char[end - start];
		strBuilder.getChars(start, end, chars, 0);
		final String query = new String(chars);

		ClickableSpan clickable = new ClickableSpan() {

			public void onClick(View view) {
				if (listener != null) listener.onUpdate(span.getURL(), query);
			}

			@Override 
			public void updateDrawState(TextPaint ds) {
				super.updateDrawState(ds);
				ds.setUnderlineText(false);
			}
		};

		strBuilder.setSpan(clickable, start, end, flags);

		if (!span.getURL().equals("nothing"))
			strBuilder.setSpan(new ForegroundColorSpan(Color.RED), start, end, flags);

		strBuilder.removeSpan(span);
	}


	public interface UpdateListener {

		public void onUpdate(String url, String query);
	}
}
