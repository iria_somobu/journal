package localhost.iillyyaa2033.journal;

import localhost.iillyyaa2033.journal.model.FontsOverride;
import android.preference.PreferenceManager;

public final class Application extends android.app.Application {

	@Override
    public void onCreate() {
        super.onCreate();
		String font = PreferenceManager.getDefaultSharedPreferences(this).getString("UI_FONT","pelagiad.ttf");
//		String font = "pelagiad.ttf";
		FontsOverride.setDefaultFont(this, "DEFAULT", font);
        FontsOverride.setDefaultFont(this, "MONOSPACE", font);
    }
}
